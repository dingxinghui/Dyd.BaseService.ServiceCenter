﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.Service.Protocol;
using XXF.BaseService.ServiceCenter.SystemRuntime;
using XXF.Extensions;

namespace XXF.BaseService.ServiceCenter.Client.Sdk
{
    /// <summary>
    /// SDK提供类
    /// </summary>
    public class SDKProvider
    {
        public void ToLocalFile(string servicenamespace, string tofiledir)
        {
            try
            {
                tofiledir = tofiledir.TrimEnd('/') + "/";
                var clientprovider = new Provider.ClientProviderPool().Get(servicenamespace);
                var protocal = clientprovider.GetServiceProtocal(); string ServiceNameSpace = clientprovider.GetServiceNameSpace();
                var servicetype = clientprovider.GetServiceType();
                if (servicetype == SystemRuntime.EnumServiceType.Thrift)
                {
                    new ThriftSDKProvider().ToLocalFile(ServiceNameSpace, tofiledir);
                }
            }
            catch (Exception exp)
            {
                LogHelper.Error(-1, EnumLogType.Client, string.Format("将系统协议转成本地代码出错,服务命名空间:{0}", servicenamespace.NullToEmpty()), exp);
                throw exp;
            }
        }
    }
}
