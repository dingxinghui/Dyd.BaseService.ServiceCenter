using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ServiceCenter.Model
{
    /// <summary>
    /// tb_log Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_log_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
        
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int serviceid { get; set; }
        
        /// <summary>
        /// 日志类型:1=服务端,2=客户端,3=系统
        /// </summary>
        public Byte logtype { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string msg { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime createtime { get; set; }
        
    }
}