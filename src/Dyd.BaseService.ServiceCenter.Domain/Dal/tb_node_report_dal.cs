﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ServiceCenter.Domain.Model;

namespace Maticsoft.DAL
{
    //tb_node_report_20150828
    public partial class tb_node_report_dal
    {
        #region C

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_node_report_model model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into tb_node_report_20150828(");
            strSql.Append("nodeid,createtime,errorcount,connectioncount,visitcount,processthreadcount,processcpuper,memorysize");
            strSql.Append(") values (");
            strSql.Append("@nodeid,@createtime,@errorcount,@connectioncount,@visitcount,@processthreadcount,@processcpuper,@memorysize");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            par.Add(new ProcedureParameter("@nodeid", model.nodeid));
            par.Add(new ProcedureParameter("@createtime", model.createtime));
            par.Add(new ProcedureParameter("@errorcount", model.errorcount));
            par.Add(new ProcedureParameter("@connectioncount", model.connectioncount));
            par.Add(new ProcedureParameter("@visitcount", model.visitcount));
            par.Add(new ProcedureParameter("@processthreadcount", model.processthreadcount));
            par.Add(new ProcedureParameter("@processcpuper", model.processcpuper));
            par.Add(new ProcedureParameter("@memorysize", model.memorysize));


            object obj = conn.ExecuteScalar(strSql.ToString(), par);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn conn, tb_node_report_model model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tb_node_report_20150828 set ");

            strSql.Append(" nodeid = @nodeid , ");
            strSql.Append(" createtime = @createtime , ");
            strSql.Append(" errorcount = @errorcount , ");
            strSql.Append(" connectioncount = @connectioncount , ");
            strSql.Append(" visitcount = @visitcount , ");
            strSql.Append(" processthreadcount = @processthreadcount , ");
            strSql.Append(" processcpuper = @processcpuper , ");
            strSql.Append(" memorysize = @memorysize  ");
            strSql.Append(" where id=@id ");

            par.Add(new ProcedureParameter("@id", model.id));
            par.Add(new ProcedureParameter("@nodeid", model.nodeid));
            par.Add(new ProcedureParameter("@createtime", model.createtime));
            par.Add(new ProcedureParameter("@errorcount", model.errorcount));
            par.Add(new ProcedureParameter("@connectioncount", model.connectioncount));
            par.Add(new ProcedureParameter("@visitcount", model.visitcount));
            par.Add(new ProcedureParameter("@processthreadcount", model.processthreadcount));
            par.Add(new ProcedureParameter("@processcpuper", model.processcpuper));
            par.Add(new ProcedureParameter("@memorysize", model.memorysize));

            int rows = conn.ExecuteSql(strSql.ToString(), par);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn conn, int id)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tb_node_report_20150828 ");
            strSql.Append(" where id=@id");
            int rows = conn.ExecuteSql(strSql.ToString(), par);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Q
        /// <summary>
        /// 重复规则
        /// </summary>
        public bool IsExists(DbConn conn, tb_node_report_model model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select top 1 id from tb_node_report_model s where 1=1");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, stringSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_node_report_model Get(DbConn conn, int id)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id, nodeid, createtime, errorcount, connectioncount, visitcount, processthreadcount, processcpuper, memorysize  ");
            strSql.Append(" from tb_node_report_20150828 ");
            strSql.Append(" where id=@id");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, strSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="search">查询实体</param>
        /// <param name="totalCount">总条数</param>
        /// <returns></returns>
        public IList<tb_node_report_model> GetPageList(DbConn conn, tb_node_report_model_search search, out int totalCount)
        {
            search.queryDay = "20150828";
            totalCount = 0;
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            IList<tb_node_report_model> list = new List<tb_node_report_model>();
            long startIndex = (search.Pno - 1) * search.PageSize + 1;
            long endIndex = startIndex + search.PageSize - 1;
            par.Add(new ProcedureParameter("startIndex", startIndex));
            par.Add(new ProcedureParameter("endIndex", endIndex));

            StringBuilder baseSql = new StringBuilder();
            string sqlWhere = " where 1=1 ";
            baseSql.Append("select row_number() over(order by id desc) as rownum,");
            #region Query
            #endregion
            baseSql.Append(" * ");
            baseSql.Append(" FROM tb_node_report_" + search.queryDay + " with(nolock)");

            string execSql = "select * from(" + baseSql.ToString() + sqlWhere + ")  as s where rownum between @startIndex and @endIndex";
            string countSql = "select count(1) from tb_node_report_" + search.queryDay + " with(nolock)" + sqlWhere;
            object obj = conn.ExecuteScalar(countSql, par);
            if (obj != DBNull.Value && obj != null)
            {
                totalCount = LibConvert.ObjToInt(obj);
            }
            DataTable dt = conn.SqlToDataTable(execSql, par);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    var model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="search">查询实体</param>
        /// <param name="totalCount">总条数</param>
        /// <returns></returns>
        public IList<tb_node_report_model> GetList(DbConn conn, tb_node_report_model_search search)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            string[] cs = search.columns.TrimEnd(',').Split(',');
            string sc = string.Empty;
            foreach (var c in cs)
            {
                sc += search.datatype + "(" + c + ") as " + c + ",";
            }

            string groupby = string.Empty;
            if (search.queryDay.Replace("-", "").Length == 8)
            {
                groupby = " group by CONVERT(varchar(30),[createtime],100)";
                sc += " CONVERT(varchar(30),[createtime],100) as createtime" + ",";
            }
            if (sc.Length > 0)
            {
                sc = sc.Substring(0, sc.Length - 1);
            }
            var table = " tb_node_report_" + search.queryDay.Replace("-", "");
            #region Query
            string sqlWhere = " where 1=1 ";
            if (search.nodeid > 0)
            {
                sqlWhere += " and nodeid=@nodeid";
                par.Add(new ProcedureParameter("nodeid", ProcParType.Int32, 4, search.nodeid));
            }
            #endregion
            string sql = " select " + sc + " from" + table + sqlWhere;
            IList<tb_node_report_model> list = new List<tb_node_report_model>();
            if (conn.TableIsExist(table.Trim()))
            {
                if (search.TimeChartType == "Day")
                {
                    string execSql = sql + groupby;

                    DataTable dt = conn.SqlToDataTable(execSql, par);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            var model = CreateModel(dr);
                            list.Add(model);
                        }
                    }
                }
            }
            return list;
        }

        #endregion

        #region private
        public tb_node_report_model CreateModel(DataRow dr)
        {
            var model = new tb_node_report_model();
            if (dr.Table.Columns.Contains("id") && dr["id"].ToString() != "")
            {
                model.id = int.Parse(dr["id"].ToString());
            }
            if (dr.Table.Columns.Contains("nodeid") && dr["nodeid"].ToString() != "")
            {
                model.nodeid = int.Parse(dr["nodeid"].ToString());
            }
            if (dr.Table.Columns.Contains("createtime") && dr["createtime"].ToString() != "")
            {
                model.createtime = DateTime.Parse(dr["createtime"].ToString());
            }
            if (dr.Table.Columns.Contains("errorcount") && dr["errorcount"].ToString() != "")
            {
                model.errorcount = int.Parse(dr["errorcount"].ToString());
            }
            if (dr.Table.Columns.Contains("connectioncount") && dr["connectioncount"].ToString() != "")
            {
                model.connectioncount = int.Parse(dr["connectioncount"].ToString());
            }
            if (dr.Table.Columns.Contains("visitcount") && dr["visitcount"].ToString() != "")
            {
                model.visitcount = long.Parse(dr["visitcount"].ToString());
            }
            if (dr.Table.Columns.Contains("processthreadcount") && dr["processthreadcount"].ToString() != "")
            {
                model.processthreadcount = int.Parse(dr["processthreadcount"].ToString());
            }
            if (dr.Table.Columns.Contains("processcpuper") && dr["processcpuper"].ToString() != "")
            {
                model.processcpuper = decimal.Parse(dr["processcpuper"].ToString());
            }
            if (dr.Table.Columns.Contains("memorysize") && dr["memorysize"].ToString() != "")
            {
                model.memorysize = decimal.Parse(dr["memorysize"].ToString());
            }

            return model;
        }
        #endregion
    }
}