﻿using Dyd.BaseService.ServiceCenter.Domain;
using Dyd.BaseService.ServiceCenter.Domain.Bll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Web.Models
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class LogExceptionAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                string controllerName = (string)filterContext.RouteData.Values["controller"];
                string actionName = (string)filterContext.RouteData.Values["action"];
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    try
                    {
                        tb_error_bll.Instance.Add(conn, string.Format("Controller:{0}|Action:{1}|Message:{2}", controllerName, actionName, filterContext.Exception.Message));
                    }
                    catch
                    {
                    }
                }
                base.OnException(filterContext);
            }
        }
    }
}